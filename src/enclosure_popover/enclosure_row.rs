use crate::util::BuilderHelper;
use gdk::AppLaunchContext;
use gio::AppInfo;
use glib::clone;
use gtk::{
    traits::{ButtonExt, WidgetExt},
    Button,
};
use libhandy::{
    traits::{ActionRowExt, PreferencesRowExt},
    ActionRow,
};
use news_flash::models::Enclosure;

pub struct EnclosureRow {
    pub widget: ActionRow,
}

impl EnclosureRow {
    pub fn new(enclosure: &Enclosure, title: &str) -> Self {
        let builder = BuilderHelper::new("enclosure_popover");
        let row = builder.get::<ActionRow>("enclosure_row");
        let invis_button = builder.get::<Button>("invis_button");

        if let Some(title) = &enclosure.title {
            row.set_title(Some(title));
        } else {
            row.set_title(Some(title));
        }

        let icon_name = if let Some(mime) = enclosure.mime_type.as_ref() {
            if mime.starts_with("image") {
                "image-x-generic-symbolic"
            } else if mime.starts_with("video") {
                "video-x-generic-symbolic"
            } else if mime.starts_with("audio") {
                "audio-x-generic-symbolic"
            } else {
                "globe-alt-symbolic"
            }
        } else {
            "globe-alt-symbolic"
        };

        let url_string = enclosure.url.to_string();
        let url_shortened = url_string.chars().take(35).chain("...".chars()).collect::<String>();

        row.set_icon_name(icon_name);
        row.set_subtitle(Some(&url_shortened));
        row.set_tooltip_text(Some(&url_string));

        invis_button.connect_activate(clone!(
            @strong enclosure => @default-panic, move |_button| {
                let url_str = enclosure.url.as_str();

                // FIXME: hope at some point opening default app for mime-type will be possible with desktop-portal
                // https://github.com/flatpak/xdg-desktop-portal/issues/574
                if let Err(error) = AppInfo::launch_default_for_uri(url_str, None::<&AppLaunchContext>) {
                    log::warn!("Failed to launch default application for uri: '{}', {}", enclosure.url, error);
                }

                // if let Some(mime_type) = &enclosure.mime_type.as_ref() {
                //     if let Some(app_info) = AppInfo::get_default_for_type(mime_type, true) {
                //         let url = vec![url_str];
                //         if app_info.launch_uris(&url, None::<&AppLaunchContext>).is_err() {
                //             log::warn!("Failed to launch default application for uri: '{}'", enclosure.url);
                //         }
                //     } else {
                //         log::warn!("No default application that handles uris for mime type: '{:?}'", enclosure.mime_type);
                //     }
                // } else {
                //     if let Err(error) = open::that(&url_str) {
                //         log::error!("Failed to open enclosure url '{}': '{}'", url_str, error);
                //     }
                // }
        }));

        EnclosureRow { widget: row }
    }
}
