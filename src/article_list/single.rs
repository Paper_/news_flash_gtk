use super::article_row::ArticleRow;
use super::models::ArticleListArticleModel;
use crate::article_list::ReadUpdate;
use crate::content_page::ContentHeader;
use crate::main_window_state::MainWindowState;
use crate::util::{BuilderHelper, GtkUtil, Util};
use crate::{app::Action, settings::Settings};
use chrono::NaiveDateTime;
use gdk::RGBA;
use glib::{clone, object::Cast, source::Continue, Sender, SourceId};
use gtk::traits::SettingsExt;
use gtk::{prelude::*, ListBox, ScrolledWindow, StateFlags, TickCallbackId};
use news_flash::models::{
    article::{Marked, Read},
    ArticleID,
};
use parking_lot::RwLock;
use std::collections::HashMap;
use std::sync::Arc;
use std::time::Duration;

const LIST_BOTTOM_THREASHOLD: f64 = 200.0;
const SCROLL_TRANSITION_DURATION: i64 = 500 * 1000;

struct ScrollAnimationProperties {
    pub start_time: Option<i64>,
    pub end_time: Option<i64>,
    pub scroll_callback_id: Option<TickCallbackId>,
    pub transition_start_value: Option<f64>,
    pub transition_diff: Option<f64>,
}

pub struct SingleArticleList {
    sender: Sender<Action>,
    scroll: ScrolledWindow,
    articles: Arc<RwLock<HashMap<ArticleID, ArticleRow>>>,
    list: ListBox,
    select_after_signal: Arc<RwLock<Option<SourceId>>>,
    scroll_cooldown: Arc<RwLock<bool>>,
    scroll_animation_data: Arc<RwLock<ScrollAnimationProperties>>,
    content_header: Arc<ContentHeader>,
}

impl SingleArticleList {
    pub fn new(sender: Sender<Action>, content_header: Arc<ContentHeader>) -> Self {
        let builder = BuilderHelper::new("article_list_single");
        let scroll = builder.get::<ScrolledWindow>("article_list_scroll");
        let list = builder.get::<ListBox>("article_list_box");

        let scroll_cooldown = Arc::new(RwLock::new(false));

        scroll.vadjustment().connect_value_changed(clone!(
            @weak scroll_cooldown,
            @strong sender => @default-panic, move |vadj|
        {
            let is_on_cooldown = *scroll_cooldown.read();
            if !is_on_cooldown {
                let max = vadj.upper() - vadj.page_size();
                if max > 0.0 && vadj.value() >= (max - LIST_BOTTOM_THREASHOLD) {
                    *scroll_cooldown.write() = true;
                    glib::source::timeout_add_local(Duration::from_millis(800), clone!(
                        @weak vadj,
                        @weak scroll_cooldown,
                        @strong sender => @default-panic, move || {
                        *scroll_cooldown.write() = false;
                        let max = vadj.upper() - vadj.page_size();
                        if max > 0.0 && vadj.value() >= (max - (LIST_BOTTOM_THREASHOLD / 4.0)) {
                            Util::send(&sender, Action::LoadMoreArticles);
                        }
                        Continue(false)
                    }));
                    Util::send(&sender, Action::LoadMoreArticles);
                }
            }
        }));

        SingleArticleList {
            sender,
            scroll,
            articles: Arc::new(RwLock::new(HashMap::new())),
            list,
            select_after_signal: Arc::new(RwLock::new(None)),
            scroll_cooldown,
            scroll_animation_data: Arc::new(RwLock::new(ScrollAnimationProperties {
                start_time: None,
                end_time: None,
                scroll_callback_id: None,
                transition_start_value: None,
                transition_diff: None,
            })),
            content_header,
        }
    }

    pub fn widget(&self) -> gtk::ScrolledWindow {
        self.scroll.clone()
    }

    pub fn list(&self) -> gtk::ListBox {
        self.list.clone()
    }

    pub fn get_background_color(&self) -> Option<RGBA> {
        let ctx = self.list.style_context();
        ctx.save();
        ctx.set_state(StateFlags::NORMAL);
        #[allow(deprecated)]
        let color = ctx.lookup_color("theme_bg_color");
        ctx.restore();
        color
    }

    pub fn add_idle(
        &self,
        article: &ArticleListArticleModel,
        pos: i32,
        state: &Arc<RwLock<MainWindowState>>,
        settings: &Arc<RwLock<Settings>>,
    ) {
        glib::idle_add_local(clone!(
            @weak state,
            @weak settings,
            @weak self.list as list,
            @strong self.articles as articles_hashmap,
            @strong article,
            @strong self.sender as sender => @default-panic, move ||
        {
            let article_row = ArticleRow::new(&article, &state, &settings, sender.clone());
            list.insert(&article_row, pos);
            article_row.show();
            articles_hashmap.write().insert(article.id.clone(), article_row);
            Continue(false)
        }));
    }

    pub fn add(
        &self,
        article: &ArticleListArticleModel,
        pos: i32,
        state: &Arc<RwLock<MainWindowState>>,
        settings: &Arc<RwLock<Settings>>,
    ) {
        let article_row = ArticleRow::new(&article, state, settings, self.sender.clone());
        self.list.insert(&article_row, pos);
        article_row.show();
        self.articles.write().insert(article.id.clone(), article_row);
    }

    pub fn remove(&self, id: &ArticleID) {
        if let Some(article_row) = self.articles.read().get(id) {
            self.list.remove(article_row);
        }
        let _article_row = self.articles.write().remove(id);
    }

    pub fn clear(&self, after_ms: Option<u64>) {
        *self.scroll_cooldown.write() = true;

        if let Some(timeout) = after_ms {
            glib::timeout_add_local(
                Duration::from_millis(timeout),
                clone!(@weak self.list as listbox => @default-panic, move || {
                    for row in listbox.children() {
                        glib::idle_add_local(clone!(@weak listbox => @default-panic, move || {
                            listbox.remove(&row);
                            Continue(false)
                        }));
                    }
                    Continue(false)
                }),
            );
        } else {
            for row in self.list.children() {
                glib::idle_add_local(clone!(@weak self.list as listbox => @default-panic, move || {
                    listbox.remove(&row);
                    Continue(false)
                }));
            }
        }

        self.articles.write().clear();
        self.scroll.vadjustment().set_value(0.0);
        *self.scroll_cooldown.write() = false;
    }

    pub fn update_date_string(&self, id: &ArticleID, date: NaiveDateTime) {
        if let Some(article_handle) = self.articles.read().get(id) {
            article_handle.update_date_string(date);
        }
    }

    pub fn get_allocated_row_height(&self, id: &ArticleID) -> Option<i32> {
        self.articles.read().get(id).map(|row| row.allocated_height())
    }

    pub fn select_after(&self, id: &ArticleID, time: u64) {
        if let Some(article_handle) = self.articles.read().get(id) {
            self.list.select_row(Some(&*article_handle));
            Util::send(
                &self.sender,
                Action::MarkArticleRead(ReadUpdate {
                    article_id: id.clone(),
                    read: Read::Read,
                }),
            );

            GtkUtil::remove_source(self.select_after_signal.write().take());

            let article_widget = article_handle.clone();
            self.select_after_signal.write().replace(glib::timeout_add_local(
                Duration::from_millis(time),
                clone!(
                    @weak self.select_after_signal as select_after_signal,
                    @weak self.content_header as content_header => @default-panic, move ||
                {
                    if content_header.is_search_focused() {
                        return Continue(false);
                    }

                    article_widget.activate();

                    select_after_signal.write().take();
                    Continue(false)
                }),
            ));
        }
    }

    pub fn animate_scroll_diff(&self, diff: f64) {
        let pos = self.get_scroll_value() + diff;
        self.animate_scroll_absolute(pos)
    }

    pub fn animate_scroll_absolute(&self, pos: f64) {
        let animate = match gtk::Settings::default() {
            Some(settings) => settings.is_gtk_enable_animations(),
            None => false,
        };

        if !self.widget().is_mapped() || !animate {
            return self.set_scroll_value(pos);
        }

        self.scroll_animation_data.write().start_time = self.widget().frame_clock().map(|clock| clock.frame_time());
        self.scroll_animation_data.write().end_time = self
            .widget()
            .frame_clock()
            .map(|clock| clock.frame_time() + SCROLL_TRANSITION_DURATION);

        let callback_id = self.scroll_animation_data.write().scroll_callback_id.take();

        let leftover_scroll = match callback_id {
            Some(callback_id) => {
                let start_value = Util::some_or_default(self.scroll_animation_data.read().transition_start_value, 0.0);
                let diff_value = Util::some_or_default(self.scroll_animation_data.read().transition_diff, 0.0);

                callback_id.remove();
                start_value + diff_value - self.get_scroll_value()
            }
            None => 0.0,
        };

        self.scroll_animation_data
            .write()
            .transition_diff
            .replace(if (pos + 1.0).abs() < 0.001 {
                self.get_scroll_upper() - self.get_scroll_page_size() - self.get_scroll_value()
            } else {
                (pos - self.get_scroll_value()) + leftover_scroll
            });

        self.scroll_animation_data
            .write()
            .transition_start_value
            .replace(self.get_scroll_value());

        self.scroll_animation_data
            .write()
            .scroll_callback_id
            .replace(self.scroll.add_tick_callback(clone!(
                @weak self.scroll_animation_data as scroll_animation_data => @default-panic, move |widget, clock| {
                let scroll = widget
                    .clone()
                    .downcast::<ScrolledWindow>()
                    .expect("Scroll tick not on ScrolledWindow");

                let start_value = Util::some_or_default(scroll_animation_data.read().transition_start_value, 0.0);
                let diff_value = Util::some_or_default(scroll_animation_data.read().transition_diff, 0.0);
                let now = clock.frame_time();
                let end_time_value = Util::some_or_default(scroll_animation_data.read().end_time, 0);
                let start_time_value = Util::some_or_default(scroll_animation_data.read().start_time, 0);

                if !widget.is_mapped() {
                    Self::set_scroll_value_static(&scroll, start_value + diff_value);
                    return Continue(false);
                }

                if scroll_animation_data.read().end_time.is_none() {
                    return Continue(false);
                }

                let t = if now < end_time_value {
                    (now - start_time_value) as f64 / (end_time_value - start_time_value) as f64
                } else {
                    1.0
                };

                let t = Util::ease_out_cubic(t);

                Self::set_scroll_value_static(&scroll, start_value + (t * diff_value));

                if scroll.vadjustment().value() <= 0.0 || now >= end_time_value {
                    scroll.queue_draw();
                    let mut scroll_animation_data_guard = scroll_animation_data.write();
                    scroll_animation_data_guard.transition_start_value.take();
                    scroll_animation_data_guard.transition_diff.take();
                    scroll_animation_data_guard.start_time.take();
                    scroll_animation_data_guard.end_time.take();
                    scroll_animation_data_guard.scroll_callback_id.take();
                    return Continue(false);
                }

                Continue(true)
            })));
    }

    fn set_scroll_value(&self, pos: f64) {
        Self::set_scroll_value_static(&self.scroll, pos)
    }

    fn set_scroll_value_static(scroll: &ScrolledWindow, pos: f64) {
        let vadjustment = scroll.vadjustment();
        let pos = if (pos + 1.0).abs() < 0.001 {
            vadjustment.upper() - vadjustment.page_size()
        } else {
            pos
        };
        vadjustment.set_value(pos);
    }

    fn get_scroll_value(&self) -> f64 {
        self.scroll.vadjustment().value()
    }

    fn get_scroll_upper(&self) -> f64 {
        self.scroll.vadjustment().upper()
    }

    fn get_scroll_page_size(&self) -> f64 {
        self.scroll.vadjustment().page_size()
    }

    pub fn get_selected_row(&self) -> Option<ArticleRow> {
        self.list
            .selected_row()
            .map(|row| row.clone().downcast::<ArticleRow>().expect("Failed to cast ArticleRow"))
    }

    pub fn set_article_row_state(&self, article_id: &ArticleID, read: Option<Read>, marked: Option<Marked>) {
        if let Some(article_row) = self.articles.read().get(article_id) {
            if let Some(read) = read {
                article_row.update_unread(read);
            }
            if let Some(marked) = marked {
                article_row.update_marked(marked);
            }
        }
    }
}
