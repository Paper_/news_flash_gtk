use crate::util::datetime_serde;
use chrono::NaiveDateTime;
use diffus::{Diffable, Same};
use news_flash::models::{Article, ArticleID, Feed, FeedID, Marked, Read, Url};
use serde::Serialize;

#[derive(Debug, Clone, Serialize)]
pub struct ArticleListArticleModel {
    pub id: ArticleID,
    pub title: String,
    pub feed_id: FeedID,
    pub feed_title: String,
    #[serde(with = "datetime_serde")]
    pub date: NaiveDateTime,
    pub summary: String,
    pub read: Read,
    pub marked: Marked,
    #[serde(skip_serializing)]
    pub url: Option<Url>,
}

impl ArticleListArticleModel {
    pub fn new(article: Article, feed: &Feed) -> Self {
        let (article_id, title, _author, feed_id, url, date, summary, _direction, read, marked, _thumbnail_url) =
            article.decompose();

        ArticleListArticleModel {
            id: article_id,
            title: match title {
                Some(title) => title,
                None => "No Title".to_owned(),
            },
            feed_id,
            feed_title: feed.label.clone(),
            date,
            summary: match summary {
                Some(summary) => summary,
                None => "No Summary".to_owned(),
            },
            read,
            marked,
            url,
        }
    }
}

impl PartialEq for ArticleListArticleModel {
    fn eq(&self, other: &ArticleListArticleModel) -> bool {
        self.id == other.id
    }
}

impl Same for ArticleListArticleModel {
    fn same(&self, other: &Self) -> bool {
        self.id == other.id //&& self.date == other.date && self.read == other.read && self.marked == other.marked
    }
}

impl<'a> Diffable<'a> for ArticleListArticleModel {
    type Diff = ArticleDiff;

    fn diff(&'a self, other: &'a Self) -> diffus::edit::Edit<Self> {
        let date = if self.date == other.date {
            None
        } else {
            Some(other.date)
        };

        let read = if self.read == other.read {
            None
        } else {
            Some(other.read)
        };

        let marked = if self.marked == other.marked {
            None
        } else {
            Some(other.marked)
        };

        if self == other && date.is_none() && read.is_none() && marked.is_none() {
            diffus::edit::Edit::Copy(self)
        } else {
            diffus::edit::Edit::Change(ArticleDiff {
                id: self.id.clone(),
                read,
                marked,
                date,
            })
        }
    }
}

pub struct ArticleDiff {
    pub id: ArticleID,
    pub read: Option<Read>,
    pub marked: Option<Marked>,
    pub date: Option<NaiveDateTime>,
}
